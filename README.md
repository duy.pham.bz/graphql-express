# graphql-express

Using Express.js for the back-end to run a server GraphQL with data is a small file JSON. The structure of the file JSON is {id, name, email, age}.

# Port

You can change to the port to match with your system in the variable `PORT` this file: `server.js`:

![img](./images/port.png)

# Running this Project

This project must be run by command line. And after that I will present step-by-step which can run this project:

1. Firstly, you should run `npm i` to install necessary packages to run this project.

2. Secondly, you must be run json-server by this command line:

```
    npm run json
```

And this is a result after run a command line above:

![img](./images/json.png)

3. Thirdly, you must be run the server Express to interact Graphiql. And this is a command line which can run the server Express:

```
    npm run dev
```

And this is a result after run a command line above:

![img](./images/server.png)

4. Fourthly, you will open your Browser and access the URL: `http://localhost:5000/graphql`. And this is a result after you access the URL above:

![img](./images/browser.png)

5. Finally, this is queries API for GraphQL to CRUD file JSON:

   - Get all customers with expect fields:

   ![img](./images/customers.png)

   - Get a customer with their id:

   ![img](./images/customer.png)

   - Add a customer & Get some his/hers info:

   ![img](./images/add.png)

   - Delete a customer and when you get info of the customer, it will be null. This mainly because the customer's info deleted.

   ![img](./images/delete.png)

   - Edit customer's info but you must give the method customer's id and some fields which want to change:

   ![img](./images/edit.png)
