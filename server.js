const express = require("express");
const { graphqlHTTP } = require("express-graphql");
const cors = require("cors");
import schema from "./schema.js";

const app = express();

// enable `cors` to set HTTP response header: Access-Control-Allow-Origin: *
app.use(cors());

app.use("/graphql", graphqlHTTP({ schema, graphiql: true }));

// Change PORT
const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}...`);
});
